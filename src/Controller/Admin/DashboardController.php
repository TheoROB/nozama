<?php

namespace App\Controller\Admin;

use App\Entity\Product;
use App\Entity\Tags;
use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{

    public function __construct(
        private AdminUrlGenerator $adminUrlGenerator
    ){

    }
    #[Route('/admin', name: 'admin')]
    public function index(): Response
    {
        $url = $this->adminUrlGenerator
            ->setController(ProductCrudController::class) 
            ->generateUrl();

        return $this->redirect($url);
        // return parent::index();
        // return $this->redirect($adminUrlGenerator->setController(OneOfYourCrudController::class)->generateUrl());
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Nozama');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::section('Site');

        yield MenuItem::subMenu('Products', 'fas fa-bars')->setSubItems([
            MenuItem::linkToCrud('Create product', 'fas fa-plus', Product::class)->setAction(Crud::PAGE_NEW),
            MenuItem::linkToCrud('Show products', 'fas fa-eye', Product::class)
        ]);

        yield MenuItem::subMenu('Clients', 'fas fa-bars')->setSubItems([
            MenuItem::linkToCrud('Create Client', 'fas fa-plus', User::class)->setAction(Crud::PAGE_NEW),
            MenuItem::linkToCrud('Show Clients', 'fas fa-eye', User::class)
        ]);

        yield MenuItem::subMenu('Tags', 'fas fa-bars')->setSubItems([
            MenuItem::linkToCrud('Create Tags', 'fas fa-plus', Tags::class)->setAction(Crud::PAGE_NEW),
            MenuItem::linkToCrud('Show Tags', 'fas fa-eye', Tags::class)
        ]);
    }
}
